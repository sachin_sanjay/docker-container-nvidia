# notes
***

## working with docker gui 

**nvidia-container-toolkit**

[nvidia-docker](https://github.com/NVIDIA/nvidia-docker/wiki/Installation-(Native-GPU-Support)) **THIS MUST BE DONE** (i have built from source but i guess that failed so i used yay to install since it is AUR'd)
## running the container
use [docker load](https://docs.docker.com/engine/reference/commandline/load/) 
### docker commit
[docker commit](https://docs.docker.com/engine/reference/commandline/commit/) is required after change

***

## AFTER LOADING THE DOCKER IMAGE RUN THE BELOW COMMAND 

```-v /tmp/.X11-unix:/tmp/.X11-unix -e DISPLAY -e XAUTHORITY -e NVIDIA_DRIVER_CAPABILITIES=all --gpus all ``` **should be the arguments to the docker run** 

after that in the host system in the terminal type

```xhost local:root``` 
(to give permissions to use xorg servers otherewise it cannot initlize) 

## full command used is 

```docker run -itd -v /tmp/.X11-unix:/tmp/.X11-unix:ro -e DISPLAY -e XAUTHORITY -e NVIDIA_DRIVER_CAPABILITIES=all --gpus all <image_name>```

***

##SIMULATION	
http://gazebosim.org/tutorials?tut=ros2_installing&cat=connect_ros (testingphase)
***
##UPDATE
in order to use qgc use the following argumetns
```--cap-add SYS_ADMIN --device /dev/fuse``` (credits rustyx)